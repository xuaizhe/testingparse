import React, { Component } from 'react';
import { StyleSheet, Text, View,TextInput, Button } from 'react-native';
import { AsyncStorage } from 'react-native';
import Parse from 'parse/react-native';

Parse.setAsyncStorage(AsyncStorage);


Parse.serverURL='https://parseapi.back4app.com/'
Parse.initialize("mG8K0wtNtND6u7j3IquGXSgLz2giiTQT2R6BtPET","jHGMs2cdXnWNAkfS8JbeLZm7mAmUiouP37jpOF34");



export default class App extends Component {

    state={
      userInput:'',
      secondInput:'',
    }
   
  
 
  onPress=()=>{
    const MyFirstClass = Parse.Object.extend("MyFirstClass");
    const myFirstClass = new MyFirstClass();

    let variable = this.state.userInput;
    let secondInput = this.state.secondInput;
   
    myFirstClass.set("name", variable);
    myFirstClass.set("class", secondInput)

    myFirstClass.save()
    .then((object) => {
      // Success
      alert('New object created with objectId: ' + object.id +' name '+ variable +' class '+secondInput);
    }, (error) => {
      // Save fails
      alert('Failed to create new object, with error code: ' + error.message);
    });
  
  }
  render(){

    return (
      <View style={styles.container}>
        <Text>Test</Text>
        <TextInput placeholder="stuff" onChangeText={userInput=>this.setState({userInput})}/>
        <TextInput placeholder="stuff" onChangeText={secondInput=>this.setState({secondInput})}/>
        <View>
        <Button title="Enter stuff" onPress={this.onPress}/>
        </View>
      </View>
      
    );
  }

 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
